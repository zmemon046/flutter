import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  static const String _title = 'Events';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      debugShowCheckedModeBanner: false,
      home: MyStatefulWidget(),
    );
  }
}

Widget image_slider_carousel = Container(
  height: 150,
  child: Carousel(
    boxFit: BoxFit.fill,
    images: [
      AssetImage('images/1.jpg'),
      AssetImage('images/2.jpg'),
      AssetImage('images/3.jpg'),
      AssetImage('images/4.jpg'),
      AssetImage('images/5.jpg'),
      AssetImage('images/6.jpg'),
      AssetImage('images/7.jpg'),
      AssetImage('images/8.jpg'),
      AssetImage('images/9.jpg'),
    ],
    autoplay: true,
    indicatorBgPadding: 1.0,
    dotColor: Colors.amber[800],
    dotSize: 5.0,
  ),
);

class MyStatefulWidget extends StatefulWidget {
  MyStatefulWidget({Key key}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  int _selectedIndex = 0;
  final tabs = [
    Center(
      child: Column(
        children: [
          Center(
            child: Text(
              "Recomended Events...",
              style: TextStyle(
                  fontWeight: FontWeight.bold, color: Colors.amber[800]),
            ),
          ),
          image_slider_carousel,
          Text(
            "Home",
            style: TextStyle(
                fontWeight: FontWeight.bold, color: Colors.amber[800]),
          ),
        ],
      ),
    ),
    Center(
      child: Text(
        "Joined",
        style: TextStyle(fontWeight: FontWeight.bold, color: Colors.amber[800]),
      ),
    ),
    Center(
      child: Text(
        "Favourites",
        style: TextStyle(fontWeight: FontWeight.bold, color: Colors.amber[800]),
      ),
    ),
    Center(
      child: Text(
        "Profile",
        style: TextStyle(fontWeight: FontWeight.bold, color: Colors.amber[800]),
      ),
    ),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          FlatButton(
            onPressed: () {
              showModalBottomSheet<void>(
            context: context,
            builder: (BuildContext context) {
              return Container(
                height: 200,
                color: Colors.white,
                child: Center(
                  child: Column(
                    // children: [

                    //   FlatButton(onPressed: (){
                    //     Navigator.pop(context);
                    //   }, child: Icon(Icons.close),)

                    // ],
                    mainAxisAlignment: MainAxisAlignment.center,
                    // mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      RaisedButton(
                        child: Text('Login with Google'),
                        onPressed: () => Navigator.pop(context),
                      ),
                      RaisedButton(
                        child: Text('Login with Facebook'),
                        onPressed: () => Navigator.pop(context),
                      ),
                    ],
                  ),
                ),
              );
            },
          );
            },
            child: Text(
              "Login",
              style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
            ),
          ),
        ],
        title: const Text('Events'),
        backgroundColor: Colors.amber[800],
      ),
      body: tabs[_selectedIndex],
      floatingActionButton: FloatingActionButton(
        mini: true,
        backgroundColor: Colors.amber[800],
        child: Icon(
          Icons.add,
          color: Colors.white,
          size: 24.0,
        ),
        onPressed: () {
          
        },
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.event),
            label: 'Joined',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            label: 'Favourites',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Profile',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: _onItemTapped,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
